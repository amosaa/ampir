<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>OOO "АМПИР" : Главная</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css" type="text/css" media="screen" />
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css"/>
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- BEGAIN PRELOADER -->
<div id="preloader">
    <div class="loader">&nbsp;</div>
</div>
<!-- END PRELOADER -->

<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
<!-- END SCROLL TOP BUTTON -->

<!-- Start header section -->
<header id="header">
    <div class="header-inner">
        <!-- Header image -->
        <img src="assets/images/header-bg.jpg" alt="img">
        <div class="header-overlay">
            <div class="header-content">
                <!-- Start header content slider -->
                <h2 class="header-slide">ООО "Ампир"
                    <span>Административное</span>
                    <span>Жилищное</span>
                    <span>Земельное право</span>
                    <span>Брачный договор</span>
                    Юридическая помощь</h2>
                <!-- End header content slider -->
                <!-- Header btn area -->
                <div class="header-btn-area">
                    <a class="knowmore-btn" href="#">Узнать большее</a>
                    <a class="download-btn" href="#">Контакты</a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End header section -->

<!-- Start menu section -->
<section id="menu-area">
    <nav class="navbar navbar-default main-navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- LOGO -->
                <a class="navbar-brand logo" href="index.html"><img src="assets/images/logo.jpg" alt="logo"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul id="top-menu" class="nav navbar-nav main-nav menu-scroll">
                    <li class="active"><a href="index.html">Главная</a></li>
                    <li><a href="#about">О нас</a></li>
                    <li><a href="#team">Команда</a></li>
                    <li><a href="#service">Услуги</a></li>
                    <li><a href="#portfolio">Портфолио</a></li>
                    <li><a href="#pricing-table">Цены </a></li>
                    <li><a href="#from-blog">Блог </a></li>
                    <li><a href="#contact">Контакты</a></li>
                </ul>
            </div><!--/.nav-collapse -->
            <div class="search-area">
                <form action="">
                    <input id="search" name="search" type="text" placeholder="What're you looking for ?">
                    <input id="search_submit" value="Rechercher" type="submit">
                </form>
            </div>
        </div>
    </nav>
</section>
<!-- End menu section -->

<!-- Start about section -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Start welcome area -->
                <div class="welcome-area">
                    <div class="title-area">
                        <h2 class="tittle">Добро пожаловать в <span>Ампир</span></h2>
                        <span class="tittle-line"></span>
                        <p>Мы предоставляем все виды юридических услуг, кроме уголовных.</p>
                    </div>
                    <div class="welcome-content">
                        <ul class="wc-table">
                            <li>
                                <div class="single-wc-content wow fadeInUp">
                                    <span class="fa fa-users wc-icon"></span>
                                    <h4 class="wc-tittle">Креативность</h4>
                                    <p>Инновационный, современный и индивидуальный подход к каждому делу </p>
                                </div>
                            </li>
                            <li>
                                <div class="single-wc-content wow fadeInUp">
                                    <span class="fa fa-sellsy wc-icon"></span>
                                    <h4 class="wc-tittle">Индивидульный подход</h4>
                                    <p>Мы рассмотрим все ваши предложения и испольним все ваши мечты </p>
                                </div>
                            </li>
                            <li>
                                <div class="single-wc-content wow fadeInUp">
                                    <span class="fa fa-line-chart wc-icon"></span>
                                    <h4 class="wc-tittle">Стратегия</h4>
                                    <p>Собственная (и довольно успешная) стратегия ведения дел </p>
                                </div>
                            </li>
                            <li>
                                <div class="single-wc-content wow fadeInUp">
                                    <span class="fa fa-bus wc-icon"></span>
                                    <h4 class="wc-tittle">Бизнес решения</h4>
                                    <p>Мы подберем оптимальный тариф для вашего бизнеса</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End welcome area -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="about-area">
                    <div class="row">
                        <div class="col-md-5 col-sm-6 col-xs-12">
                            <div class="about-left wow fadeInLeft">
                                <img src="assets/images/about-img.jpg" alt="img">
                                <a class="introduction-btn" href="#">Introduction</a>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                            <div class="about-right wow fadeInRight">
                                <div class="title-area">
                                    <h2 class="tittle">О компании <span>Ампир</span></h2>
                                    <span class="tittle-line"></span>
                                    <p>На рынке юридических услуг с 2010 года</p>

                                    <p>Более 50-ти выйгранных дел</p>
                                    <div class="about-btn-area">
                                        <a href="#" class="button button-default" data-text="KNOW MORE"><span>Узнать больше</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End about section -->

<!-- Start call to action -->
<section id="call-to-action">
    <img src="assets/images/call-to-action-bg.png" alt="img">
    <div class="call-to-overlay">
        <div class="container">
            <div class="call-to-content wow fadeInUp">
                <h2>Это лучшая Юридичиская Компания города Петрозаводска</h2>
                <a href="#" class="button button-default" data-text="GET IT NOW"><span>Заказать звонок</span></a>
            </div>
        </div>
    </div>
</section>
<!-- End call to action -->

<!-- Start Team action -->
<section id="team">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="team-area">
                    <div class="title-area">
                        <h2 class="tittle">Наша команда</h2>
                        <span class="tittle-line"></span>
                        <p>Самые опытные юристы. Директор компании - майор в отставке</p>
                    </div>
                    <!-- Start team content -->
                    <div class="team-content">
                        <ul class="team-grid">
                            <li>
                                <div class="team-item team-img-1 wow fadeInUp">
                                    <div class="team-info">
                                        <p> Я решу любые ваши проблемы</p>
                                        <a href="#"><span class="fa fa-facebook"></span></a>
                                        <a href="#"><span class="fa fa-twitter"></span></a>
                                        <a href="#"><span class="fa fa-pinterest"></span></a>
                                        <a href="#"><span class="fa fa-rss"></span></a>
                                    </div>
                                </div>
                                <div class="team-address">
                                    <p>Истомина Нина Александровна</p>
                                    <span>Директор</span>
                                </div>
                            </li>
                            <li>
                                <div class="team-item team-img-2 wow fadeInUp">
                                    <div class="team-info">
                                        <p> I must explain to you how all this mistaken idea of denouncing pleasure n</p>
                                        <a href="#"><span class="fa fa-facebook"></span></a>
                                        <a href="#"><span class="fa fa-twitter"></span></a>
                                        <a href="#"><span class="fa fa-pinterest"></span></a>
                                        <a href="#"><span class="fa fa-rss"></span></a>
                                    </div>
                                </div>
                                <div class="team-address">
                                    <p>Ольга</p>
                                    <span>Заместитель директора</span>
                                </div>
                            </li>
                            <li>
                                <div class="team-item team-img-3 wow fadeInUp">
                                    <div class="team-info">
                                        <p> Юрист с десятилетним стажем</p>
                                        <a href="#"><span class="fa fa-facebook"></span></a>
                                        <a href="#"><span class="fa fa-twitter"></span></a>
                                        <a href="#"><span class="fa fa-pinterest"></span></a>
                                        <a href="#"><span class="fa fa-rss"></span></a>
                                    </div>
                                </div>
                                <div class="team-address">
                                    <p>Юлия</p>
                                    <span>Старший юрист</span>
                                </div>
                            </li>
                            <li>
                                <div class="team-item team-img-4 wow fadeInUp">
                                    <div class="team-info">
                                        <p> Юрист с пятилетним стажем</p>
                                        <a href="#"><span class="fa fa-facebook"></span></a>
                                        <a href="#"><span class="fa fa-twitter"></span></a>
                                        <a href="#"><span class="fa fa-pinterest"></span></a>
                                        <a href="#"><span class="fa fa-rss"></span></a>
                                    </div>
                                </div>
                                <div class="team-address">
                                    <p>Мария</p>
                                    <span>Юрист</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- End team content -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start Team action -->

<!-- Start service section -->
<section id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="service-area">
                    <div class="title-area">
                        <h2 class="tittle">Наши услуги</h2>
                        <span class="tittle-line"></span>
                        <p>Мы предоставляем широкий спектр юридических услуг</p>
                    </div>
                    <!-- service content -->
                    <div class="service-content">
                        <ul class="service-table">
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-edit service-icon"></span>
                                    <h4 class="service-title">Арбитражные</h4>
                                    <p>Арбитражные дела</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-sort-amount-asc service-icon"></span>
                                    <h4 class="service-title">Гражданские</h4>
                                    <p>Гражданские дела</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-map-o service-icon"></span>
                                    <h4 class="service-title">Семейные</h4>
                                    <p>Семейные дела</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-rocket service-icon"></span>
                                    <h4 class="service-title">Купля-продажа</h4>
                                    <p>Заключение договоров</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-car service-icon"></span>
                                    <h4 class="service-title">Исковое заявление</h4>
                                    <p>Составление искового заявления в суд</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-bank service-icon"></span>
                                    <h4 class="service-title">Представительство</h4>
                                    <p>Представительство во всех судах города Петрозаводска</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-user-secret service-icon"></span>
                                    <h4 class="service-title">Составление договоров</h4>
                                    <p>Составление любых видов договоров</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-support service-icon"></span>
                                    <h4 class="service-title">Открытие, регистрация</h4>
                                    <p>Открытие, регистрация предприятий, ИП, ООО</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-car service-icon"></span>
                                    <h4 class="service-title">Ликвидация ООО - ИП</h4>
                                    <p>Полная ликвидация компаний из всех государственных реестров</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-bank service-icon"></span>
                                    <h4 class="service-title">Автомобильные</h4>
                                    <p>Разрешение всех автомобильных споров, возмещение с виновника авварии</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-user-secret service-icon"></span>
                                    <h4 class="service-title">Консультация</h4>
                                    <p>Также мы оказываем платную консультацию</p>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-6">
                                <div class="single-service wow slideInUp">
                                    <span class="fa fa-support service-icon"></span>
                                    <h4 class="service-title">И многое другое</h4>
                                    <p>И это не все, что мы можем</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End service section -->

<!-- Start Portfolio section -->



<!-- Start counter section -->
<section id="counter">
    <img src="assets/images/counter-bg.jpg" alt="img">
    <div class="counter-overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Start counter area -->
                    <div class="counter-area">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single-counter">
                                <span class="fa fa-users"></span>
                                <div class="counter-count">
                                    <span class="counter">32</span>
                                    <p>Счастливых клиента за 2 года</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single-counter">
                                <span class="fa fa-bus"></span>
                                <div class="counter-count">
                                    <span class="counter">5</span>
                                    <p>Проектов в работе</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single-counter">
                                <span class="fa fa-twitter"></span>
                                <div class="counter-count">
                                    <span class="counter">15</span>
                                    <p>Клиентов на абонементе</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single-counter">
                                <span class="fa fa-tasks"></span>
                                <div class="counter-count">
                                    <span class="counter">57</span>
                                    <p>Успешных проектов</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End counter section -->

<!-- Start Pricing Table section -->
<section id="pricing-table">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pricing-table-area">
                    <div class="title-area">
                        <h2 class="tittle">Стоимость услуг</h2>
                        <span class="tittle-line"></span>
                        <p>Юридичекая компания "Ампир" предоставляет широкий спектр качественных и не дорогих услуг</p>
                    </div>
                    <!-- service content -->
                    <div class="pricing-table-content">
                        <ul class="price-table">
                            <li class="wow slideInUp">
                                <div class="single-price">
                                    <h4 class="price-header">Исковое заявление</h4>
                                    <span class="price-amount">700 рублей</span>
                                    <a data-text="Заказать услугу" class="button button-default" href="#"><span>Заказать</span></a>
                                </div>
                            </li>
                            <li class="wow slideInUp">
                                <div class="single-price">
                                    <h4 class="price-header">Консультация</h4>
                                    <span class="price-amount">500 рублей</span>
                                    <a data-text="Заказать услугу" class="button button-default" href="#"><span>Заказать</span></a>
                                </div>
                            </li>
                            <li class="wow slideInUp">
                                <div class="single-price">
                                    <h4 class="price-header">Абонемент</h4>
                                    <span class="price-amount">5000 р/мес.</span>
                                    <a data-text="Заказать услугу" class="button button-default" href="#"><span>Заказать</span></a>
                                </div>
                            </li>
                            <li class="wow slideInUp">
                                <div class="single-price">
                                    <h4 class="price-header">Ликвидация ООО</h4>
                                    <span class="price-amount">10000 рублей</span>
                                    <a data-text="Заказать услугу" class="button button-default" href="#"><span>Заказать</span></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pricing Table section -->

<!-- Start Testimonial section -->
<section id="testimonial">
    <img src="assets/images/testimonial-bg.jpg" alt="img">
    <div class="counter-overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Start Testimonial area -->
                    <div class="testimonial-area">
                        <div class="title-area">
                            <h2 class="tittle">Что клиенты говорят о нас</h2>
                            <span class="tittle-line"></span>
                        </div>
                        <div class="testimonial-conten">
                            <!-- Start testimonial slider -->
                            <div class="testimonial-slider">
                                <!-- single slide -->
                                <div class="single-slide">
                                    <p>В компании "Ампир" только опытные юристы. Они довели мое дело до конца и мой бизнес получил новый толчок к развитию</p>
                                    <div class="single-testimonial">
                                        <img class="testimonial-thumb" src="assets/images/testimonial-image1.png" alt="img">
                                        <p>Иван Сидоров</p>
                                        <span>Директор 000 "Креатив"</span>
                                    </div>
                                </div>
                                <!-- single slide -->
                                <div class="single-slide">
                                    <p>Страховая компания после аварии отказывалась платить. С помощью компании "Ампир" я добился своего и получил назад мои деньги</p>
                                    <div class="single-testimonial">
                                        <img class="testimonial-thumb" src="assets/images/team-member2.png" alt="img">
                                        <p>Петр Иванов</p>
                                        <span>Водитель</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Testimonial section -->

<!-- Start from blog section -->
<section id="from-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="from-blog-area">
                    <div class="title-area">
                        <h2 class="tittle">Наш блог</h2>
                        <span class="tittle-line"></span>
                        <p>Последние новости компании, полезные советы и просто об о всем</p>
                    </div>
                    <!-- From Blog content -->
                    <div class="from-blog-content">
                        <div class="row">
                            <div class="col-md-4">
                                <article class="single-from-blog">
                                    <figure>
                                        <a href="blog-single.html"><img src="assets/images/the-sky.jpg" alt="img"></a>
                                    </figure>
                                    <div class="blog-title">
                                        <h2><a href="blog-single.html">Регистрация фирм в Петрозаводске</a></h2>
                                        <p>Опуликовано <a class="blog-admin" href="#">admin</a> <span class="blog-date">6-го августа 2016</span></p>
                                    </div>
                                    <p>Вариант первый: заняться регистрацией ООО самому. Кажется, что это - самый экономичный способ. Но это не так. На самом деле здесь можно не только сэкономить, но и наоборот - потерять деньги, а то и весь бизнес.</p>
                                    <div class="blog-footer">
                                        <a href="#"><span class="fa fa-comment"></span>18 Комментариев</a>
                                        <a href="#"><span class="fa fa-thumbs-o-up"></span>35 Лайков</a>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="single-from-blog">
                                    <figure>
                                        <a href="blog-single.html"><img src="assets/images/photographer.jpg" alt="img"></a>
                                    </figure>
                                    <div class="blog-title">
                                        <h2><a href="blog-single.html">Here is the post title</a></h2>
                                        <p>Опуликовано <a class="blog-admin" href="#">admin</a> <span class="blog-date">6-го августа 2016</span></p>
                                    </div>
                                    <p>Sed ut perspiciatis unde mnis is te natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis </p>
                                    <div class="blog-footer">
                                        <a href="#"><span class="fa fa-comment"></span>18 Comments</a>
                                        <a href="#"><span class="fa fa-thumbs-o-up"></span>35 Likes</a>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="single-from-blog">
                                    <figure>
                                        <a href="blog-single.html"><img src="assets/images/sealand.jpg" alt="img"></a>
                                    </figure>
                                    <div class="blog-title">
                                        <h2><a href="blog-single.html">Here is the post title</a></h2>
                                        <p>Опуликовано <a class="blog-admin" href="#">admin</a> <span class="blog-date">6-го августа 2016</span></p>
                                    </div>
                                    <p>Sed ut perspiciatis unde mnis is te natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis </p>
                                    <div class="blog-footer">
                                        <a href="#"><span class="fa fa-comment"></span>18 Comments</a>
                                        <a href="#"><span class="fa fa-thumbs-o-up"></span>35 Likes</a>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End from blog section -->


<!-- Start Contact section -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="contact-left wow fadeInLeft">
                    <h2>Наши контакты</h2>
                    <address class="single-address">
                        <h4>Почтовый адрес:</h4>
                        <p>185001, респ. Карелия, г Петрозаводск, ул. Путейская, дом 5</p>
                    </address>
                    <address class="single-address">
                        <h4>Офис:</h4>
                        <p>185000, респ. Карелия, г. Петрозаводск, ул. Володарского, дом 44, цоколь, оф. 5</p>
                    </address>
                    <address class="single-address">
                        <h4>Телефоны</h4>
                        <p>Телефон: +7 (981) 407 81 57</p>
                        <p>Fax Number: 8 8142 72 03 52</p>
                    </address>
                    <address class="single-address">
                        <h4>E-Mail</h4>
                        <p>Поддержка: support@ampir.com</p>
                        <p>Консультация: sales@ampir.com</p>
                    </address>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="contact-right wow fadeInRight">
                    <h2>Отправить сообщение</h2>
                    <form action="" class="contact-form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Enter Email">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control"></textarea>
                        </div>
                        <button type="submit" data-text="SUBMIT" class="button button-default"><span>Отправить</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact section -->
<!-- Start Google Map -->
<section id="google-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d3773.3159806164767!2d34.366988239670235!3d61.77936428009242!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e6!4m0!4m3!3m2!1d61.7794708!2d34.3687263!5e0!3m2!1sru!2sru!4v1470494449002" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
<!-- End Google Map -->

<!-- Start Footer -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-top-area">
                        <a class="footer-logo" href="#"><img src="assets/images/logo.jpg" alt="Logo"></a>
                        <div class="footer-social">
                            <a class="facebook" href="#"><span class="fa fa-facebook"></span></a>
                            <a class="twitter" href="#"><span class="fa fa-twitter"></span></a>
                            <a class="google-plus" href="#"><span class="fa fa-google-plus"></span></a>
                            <a class="youtube" href="#"><span class="fa fa-youtube"></span></a>
                            <a class="linkedin" href="#"><span class="fa fa-linkedin"></span></a>
                            <a class="dribbble" href="#"><span class="fa fa-dribbble"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p>Designed by <a href="http://www.markups.io/">MarkUps.io</a></p>
    </div>
</footer>
<!-- End Footer -->

<!-- initialize jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap -->
<script src="assets/js/bootstrap.js"></script>
<!-- Slick Slider -->
<script type="text/javascript" src="assets/js/slick.js"></script>
<!-- Counter -->
<script type="text/javascript" src="assets/js/waypoints.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="assets/js/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>
<!-- Wow animation -->
<script type="text/javascript" src="assets/js/wow.js"></script>

<!-- Custom js -->
<script type="text/javascript" src="assets/js/custom.js"></script>

</body>
</html>